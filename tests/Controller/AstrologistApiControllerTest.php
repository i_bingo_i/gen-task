<?php

declare(strict_types=1);

namespace Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AstrologistApiControllerTest extends WebTestCase
{
    public function testIndexSuccess()
    {
        $client = static::createClient();

        $client->request('GET', '/api/astrologists');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}