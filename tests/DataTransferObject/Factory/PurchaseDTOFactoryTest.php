<?php

declare(strict_types=1);

namespace DataTransferObject\Factory;

use App\DataTransferObject\Factory\PurchaseDTOFactory;
use App\DataTransferObject\PurchaseCreateDTO;
use App\Entity\Proposal;
use PHPUnit\Framework\TestCase;

class PurchaseDTOFactoryTest extends TestCase
{
    /**
     * @var PurchaseDTOFactory
     */
    private $purchaseDTOFactory;

    protected function setUp(): void
    {
        $this->purchaseDTOFactory = new PurchaseDTOFactory();
    }

    public function testCreateDTOSuccess()
    {
        $proposal = $this->createMock(Proposal::class);
        $inputData = [
            'name' => 'testname',
            'email' => 'test@gmail.com'
        ];

        $result = $this->purchaseDTOFactory->createDTO($proposal, $inputData);

        $this->assertInstanceOf(PurchaseCreateDTO::class, $result);
    }
}