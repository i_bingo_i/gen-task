<?php


namespace DataTransferObject;

use App\DataTransferObject\PurchaseCreateDTO;
use App\Entity\Proposal;
use PHPUnit\Framework\TestCase;

class PurchaseCreateDTOTest extends TestCase
{
    public function testHandlingSuccess()
    {
        /** @var  Proposal $proposal */
        $proposal = $this->createMock(Proposal::class);
        $name = 'test name';
        $email = 'test@gmail.com';

        $purchaseCreateDTO = new PurchaseCreateDTO($proposal, $name, $email);

        $this->assertEquals($proposal, $purchaseCreateDTO->getProposal());
        $this->assertEquals($name, $purchaseCreateDTO->getName());
        $this->assertEquals($email, $purchaseCreateDTO->getEmail());
    }
}