<?php

declare(strict_types=1);

namespace Factory;

use App\DataTransferObject\PurchaseCreateDTO;
use App\Entity\Proposal;
use App\Entity\Purchase;
use App\Factory\PurchaseFactory;
use PHPUnit\Framework\TestCase;

class PurchaseFactoryTest extends TestCase
{
    /**
     * @var PurchaseFactory
     */
    private $purchaseFactory;

    protected function setUp(): void
    {
        $this->purchaseFactory = new PurchaseFactory();
    }

    public function testCreate()
    {
        $purchaseCreateDTO = $this->createMock(PurchaseCreateDTO::class);
        $proposal = $this->createMock(Proposal::class);

        $purchaseCreateDTO->expects($this->once())->method('getEmail')->willReturn('test@gmail.com');
        $purchaseCreateDTO->expects($this->once())->method('getName')->willReturn('testname');
        $purchaseCreateDTO->expects($this->once())->method('getProposal')->willReturn($proposal);

        $result = $this->purchaseFactory->create($purchaseCreateDTO);

        $this->assertInstanceOf(Purchase::class, $result);
    }
}