<?php

declare(strict_types=1);

namespace App\Factory;

use App\DataTransferObject\PurchaseCreateDTO;
use App\Entity\Purchase;

interface PurchaseFactoryInterface
{
    /**
     * @param PurchaseCreateDTO $purchaseCreateDTO
     * @return Purchase
     */
    public function create(PurchaseCreateDTO $purchaseCreateDTO): Purchase;
}