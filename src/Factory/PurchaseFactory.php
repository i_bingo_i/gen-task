<?php

declare(strict_types=1);

namespace App\Factory;

use App\DataTransferObject\PurchaseCreateDTO;
use App\Entity\Purchase;

class PurchaseFactory implements PurchaseFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(PurchaseCreateDTO $purchaseCreateDTO): Purchase
    {
        $purchase = new Purchase();
        $purchase->setBuyerEmail($purchaseCreateDTO->getEmail());
        $purchase->setBuyerName($purchaseCreateDTO->getName());
        $purchase->setProposal($purchaseCreateDTO->getProposal());
        $purchase->setIsPaid();

        return $purchase;
    }
}