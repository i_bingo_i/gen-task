<?php

declare(strict_types=1);

namespace App\Manager;

use App\Entity\Astrologist;

interface AstrologistManagerInterface
{
    /**
     * @return Astrologist[]
     */
    public function index(): array;
}