<?php

declare(strict_types=1);

namespace App\Manager;

use App\Repository\AstrologistRepository;

class AstrologistManager implements AstrologistManagerInterface
{
    /**
     * @var AstrologistRepository
     */
    private $astrologistRepository;

    /**
     * AstrologistManager constructor.
     * @param AstrologistRepository $astrologistRepository
     */
    public function __construct(AstrologistRepository $astrologistRepository)
    {
        $this->astrologistRepository = $astrologistRepository;
    }

    /**
     * @inheritDoc
     */
    public function index(): array
    {
        return $this->astrologistRepository->findAll();
    }
}