<?php

declare(strict_types=1);

namespace App\Manager;

use App\Entity\Proposal;
use App\Entity\Purchase;

interface PurchaseManagerManagerInterface
{
    /**
     * @param Proposal $proposal
     * @param array $inputData
     * @return Purchase
     */
    public function store(Proposal $proposal, array $inputData): Purchase;
}