<?php

declare(strict_types=1);

namespace App\Manager;

use App\DataTransferObject\Factory\PurchaseDTOFactoryInterface;
use App\DataTransferObject\PurchaseCreateDTO;
use App\Entity\Proposal;
use App\Entity\Purchase;
use App\Factory\PurchaseFactoryInterface;
use App\Message\Factory\GoogleSheetsMessageFactoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class PurchaseManagerManager implements PurchaseManagerManagerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var PurchaseDTOFactoryInterface
     */
    private $DTOFactory;

    /**
     * @var PurchaseFactoryInterface
     */
    private $purchaseFactory;

    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var GoogleSheetsMessageFactoryInterface
     */
    private $googleSheetsMessageFactory;

    /**
     * PurchaseManagerManager constructor.
     * @param EntityManagerInterface $em
     * @param PurchaseDTOFactoryInterface $DTOFactory
     * @param PurchaseFactoryInterface $purchaseFactory
     * @param MessageBusInterface $bus
     * @param GoogleSheetsMessageFactoryInterface $googleSheetsMessageFactory
     */
    public function __construct(
        EntityManagerInterface $em,
        PurchaseDTOFactoryInterface $DTOFactory,
        PurchaseFactoryInterface $purchaseFactory,
        MessageBusInterface $bus,
        GoogleSheetsMessageFactoryInterface $googleSheetsMessageFactory
    )
    {
        $this->em = $em;
        $this->DTOFactory = $DTOFactory;
        $this->purchaseFactory = $purchaseFactory;
        $this->bus = $bus;
        $this->googleSheetsMessageFactory = $googleSheetsMessageFactory;
    }

    /**
     * @inheritDoc
     */
    public function store(Proposal $proposal, array $inputData): Purchase
    {
        /** @var PurchaseCreateDTO $purchaseCreateDTO */
        $purchaseCreateDTO = $this->DTOFactory->createDTO($proposal, $inputData);
        /** @var Purchase $purchase */
        $purchase = $this->purchaseFactory->create($purchaseCreateDTO);

        $this->em->persist($purchase);
        $this->em->flush();

        $this->bus->dispatch($this->googleSheetsMessageFactory->create($purchase->getId()));

        return $purchase;
    }
}