<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PurchaseRepository")
 * @ORM\Table(name="purchases")
 */
class Purchase
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"store"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"store"})
     *
     * @var string
     */
    private $buyerName;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"store"})
     *
     * @var string
     */
    private $buyerEmail;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Groups({"store"})
     *
     * @var bool
     */
    private $isPaid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Proposal", inversedBy="purchases")
     * @ORM\JoinColumn(name="proposal_id", referencedColumnName="id")
     *
     * @Groups({"store"})
     *
     * @var Proposal
     */
    private $proposal;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBuyerName(): string
    {
        return $this->buyerName;
    }

    /**
     * @param string $buyerName
     */
    public function setBuyerName(string $buyerName): void
    {
        $this->buyerName = $buyerName;
    }

    /**
     * @return string
     */
    public function getBuyerEmail(): string
    {
        return $this->buyerEmail;
    }

    /**
     * @param string $buyerEmail
     */
    public function setBuyerEmail(string $buyerEmail): void
    {
        $this->buyerEmail = $buyerEmail;
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->isPaid;
    }

    /**
     * @param bool $isPaid
     */
    public function setIsPaid(bool $isPaid = false): void
    {
        $this->isPaid = $isPaid;
    }

    /**
     * @return Proposal
     */
    public function getProposal(): Proposal
    {
        return $this->proposal;
    }

    /**
     * @param Proposal $proposal
     */
    public function setProposal(Proposal $proposal): void
    {
        $this->proposal = $proposal;
    }
}
