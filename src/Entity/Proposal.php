<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProposalRepository")
 * @ORM\Table(name="proposals")
 */
class Proposal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Astrologist", inversedBy="proposals")
     * @ORM\JoinColumn(name="astrologist_id", referencedColumnName="id")
     *
     * @var Astrologist
     */
    private $astrologist;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Service", inversedBy="proposals")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     *
     * @Groups({"list", "show"})
     *
     * @var Service
     */
    private $service;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups({"show", "store"})
     *
     * @var int
     */
    private $price;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return Astrologist
     */
    public function getAstrologist(): Astrologist
    {
        return $this->astrologist;
    }

    /**
     * @return Service
     */
    public function getService(): Service
    {
        return $this->service;
    }

    /**
     * @param Astrologist $astrologist
     */
    public function setAstrologist(Astrologist $astrologist): void
    {
        $this->astrologist = $astrologist;
    }

    /**
     * @param Service $service
     */
    public function setService(Service $service): void
    {
        $this->service = $service;
    }
}
