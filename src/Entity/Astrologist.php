<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AstrologistRepository")
 * @ORM\Table(name="astrologists")
 */
class Astrologist
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"list", "show"})
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"list", "show"})
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="text", length=500)
     *
     * @Groups({"list", "show"})
     *
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="text", length=500)
     *
     * @Groups({"show"})
     *
     * @var string
     */
    private $info;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Proposal", mappedBy="astrologist")
     *
     * @Groups({"list", "show"})
     *
     * @var ArrayCollection
     */
    private $proposals;

    /**
     * Astrologist constructor.
     */
    public function __construct()
    {
        $this->proposals = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPhoto(): string
    {
        return $this->photo;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getInfo(): string
    {
        return $this->info;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $photo
     */
    public function setPhoto(string $photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @param string $info
     */
    public function setInfo(string $info): void
    {
        $this->info = $info;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getProposals()
    {
        return $this->proposals;
    }
}
