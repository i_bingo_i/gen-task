<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Service;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ServiceFixture extends Fixture
{
    const SERVICE_NAMES = ["Натальная карта", "Детальный гороскоп", "Отчет совместимости", "Гороскоп на год"];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::SERVICE_NAMES as $key => $name) {
            $service = new Service();
            $service->setName($name);
            $manager->persist($service);

            $this->addReference($name . $key, $service);
        }

        $manager->flush();
    }
}
