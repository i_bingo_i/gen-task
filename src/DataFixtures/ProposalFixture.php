<?php

namespace App\DataFixtures;

use App\Entity\Proposal;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProposalFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (AstrologistFixture::NAMES as $key => $name) {
            foreach (ServiceFixture::SERVICE_NAMES as $serviceKey => $serviceName) {
                $proposal = new Proposal();
                $proposal->setAstrologistId($this->getReference($name . $key));
                $proposal->setServiceId($this->getReference($serviceName . $serviceKey));
                $proposal->setPrice(rand(100, 300));
                $manager->persist($proposal);
            }
        }

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [
            AstrologistFixture::class,
            ServiceFixture::class
        ];
    }
}
