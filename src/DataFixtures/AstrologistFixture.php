<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Astrologist;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AstrologistFixture extends Fixture
{
    const NAMES = ["Люси", "Кен", "Барбара", "Кевин", "Сьюзи", "Эмма"];
    const PHOTO = "https://www.ezo.fm/wp-content/uploads/bfi_thumb/-366s2urstxue3lzcxs06bu.jpg";
    const EMAIL = "test%s@gmail.com";
    const INFO = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";

    public function load(ObjectManager $manager)
    {
        foreach (self::NAMES as $key => $name) {
            $astrologist = new Astrologist();
            $astrologist->setName($name);
            $astrologist->setPhoto(self::PHOTO);
            $astrologist->setEmail(sprintf(self::EMAIL, $key));
            $astrologist->setInfo(self::INFO);
            $manager->persist($astrologist);

            $this->addReference($name . $key, $astrologist);
        }

        $manager->flush();
    }
}
