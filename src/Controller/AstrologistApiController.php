<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Astrologist;
use App\Manager\AstrologistManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class AstrologistApiController extends AbstractController
{
    /**
     * @var AstrologistManagerInterface
     */
    private $astrologistManager;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * AstrologistApiController constructor.
     * @param AstrologistManagerInterface $astrologistManager
     * @param SerializerInterface $serializer
     */
    public function __construct(
        AstrologistManagerInterface $astrologistManager,
        SerializerInterface $serializer
    )
    {
        $this->astrologistManager = $astrologistManager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/astrologists", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $result = $this->astrologistManager->index();

        $response = $this->serializer->serialize($result, 'json', ['groups' => ['list']]);

        return JsonResponse::fromJsonString($response);
    }

    /**
     * @Route("/api/astrologists/{astrologist}", methods={"GET"})
     *
     * @param Astrologist $astrologist
     * @return JsonResponse
     */
    public function show(Astrologist $astrologist): JsonResponse
    {
        $response = $this->serializer->serialize($astrologist, 'json', ['groups' => ['show']]);

        return JsonResponse::fromJsonString($response);
    }
}
