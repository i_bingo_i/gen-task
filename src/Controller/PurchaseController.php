<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Proposal;
use App\Manager\PurchaseManagerManagerInterface;
use App\Validator\PurchaseCreateValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class PurchaseController extends AbstractController
{
    /**
     * @var PurchaseManagerManagerInterface
     */
    private $purchaseManager;

    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var PurchaseCreateValidatorInterface
     */
    private $purchaseCreateValidator;

    /**
     * PurchaseController constructor.
     * @param PurchaseManagerManagerInterface $purchaseManager
     * @param SerializerInterface $serializer
     * @param PurchaseCreateValidatorInterface $purchaseCreateValidator
     */
    public function __construct(
        PurchaseManagerManagerInterface $purchaseManager,
        SerializerInterface $serializer,
        PurchaseCreateValidatorInterface $purchaseCreateValidator
    )
    {
        $this->purchaseManager = $purchaseManager;
        $this->serializer = $serializer;
        $this->purchaseCreateValidator = $purchaseCreateValidator;
    }

    /**
     * @Route("/api/purchases/proposals/{proposal}", methods={"POST"})
     *
     * @param Proposal $proposal
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Proposal $proposal, Request $request): JsonResponse
    {
        $inputData = json_decode($request->getContent(), true);

        $this->purchaseCreateValidator->validate($inputData);

        $result = $this->purchaseManager->store($proposal, $inputData);

        $response = $this->serializer->serialize($result, 'json', ['groups' => ['store']]);

        return JsonResponse::fromJsonString($response, Response::HTTP_CREATED);
    }
}
