<?php

declare(strict_types=1);

namespace App\DataTransferObject;

use App\Entity\Proposal;

interface PurchaseCreateDTOInterface
{
    /**
     * @return Proposal
     */
    public function getProposal(): Proposal;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getEmail(): string;
}