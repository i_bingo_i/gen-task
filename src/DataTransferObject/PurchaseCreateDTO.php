<?php

declare(strict_types=1);

namespace App\DataTransferObject;

use App\Entity\Proposal;

class PurchaseCreateDTO implements PurchaseCreateDTOInterface
{
    /**
     * @var Proposal
     */
    private $proposal;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * PurchaseCreateDTO constructor.
     * @param Proposal $proposal
     * @param string $name
     * @param string $email
     */
    public function __construct(Proposal $proposal, string $name, string $email)
    {
        $this->proposal = $proposal;
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * @inheritDoc
     */
    public function getProposal(): Proposal
    {
        return $this->proposal;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}