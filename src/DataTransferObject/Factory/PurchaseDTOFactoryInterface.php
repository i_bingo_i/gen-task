<?php

declare(strict_types=1);

namespace App\DataTransferObject\Factory;

use App\DataTransferObject\PurchaseCreateDTOInterface;
use App\Entity\Proposal;

interface PurchaseDTOFactoryInterface
{
    /**
     * @param Proposal $proposal
     * @param array $inputData
     * @return PurchaseCreateDTOInterface
     */
    public function createDTO(Proposal $proposal, array $inputData): PurchaseCreateDTOInterface;
}