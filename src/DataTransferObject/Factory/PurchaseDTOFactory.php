<?php

declare(strict_types=1);

namespace App\DataTransferObject\Factory;

use App\DataTransferObject\PurchaseCreateDTO;
use App\DataTransferObject\PurchaseCreateDTOInterface;
use App\Entity\Proposal;

class PurchaseDTOFactory implements PurchaseDTOFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createDTO(Proposal $proposal, array $inputData): PurchaseCreateDTOInterface
    {
        return new PurchaseCreateDTO(
            $proposal,
            $inputData['name'],
            $inputData['email']
        );
    }
}