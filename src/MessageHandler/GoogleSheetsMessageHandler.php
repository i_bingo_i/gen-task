<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Entity\Purchase;
use App\Message\GoogleSheetsMessageInterface;
use App\Repository\PurchaseRepository;
use App\Service\GoogleSheet\GoogleSheetClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GoogleSheetsMessageHandler implements MessageHandlerInterface
{
    /**
     * @var PurchaseRepository
     */
    private $purchaseRepository;
    /**
     * @var GoogleSheetClientInterface
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * GoogleSheetsMessageHandler constructor.
     * @param PurchaseRepository $purchaseRepository
     * @param GoogleSheetClientInterface $client
     * @param LoggerInterface $logger
     */
    public function __construct(
        PurchaseRepository $purchaseRepository,
        GoogleSheetClientInterface $client,
        LoggerInterface $logger
    )
    {
        $this->purchaseRepository = $purchaseRepository;
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @param GoogleSheetsMessageInterface $googleSheetsMessage
     * @throws
     */
    public function __invoke(GoogleSheetsMessageInterface $googleSheetsMessage)
    {
        /** @var int $purchaseId */
        $purchaseId = $googleSheetsMessage->getPurchaseId();
        /** @var Purchase $purchase */
        $purchase = $this->purchaseRepository->find($purchaseId);

        try {
            $this->client->write(
                [
                    [
                        $purchase->getId(),
                        $purchase->getBuyerName(),
                        $purchase->getBuyerEmail(),
                        $purchase->getProposal()->getPrice()
                    ]
                ]
            );

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}