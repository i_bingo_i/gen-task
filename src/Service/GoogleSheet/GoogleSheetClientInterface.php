<?php

declare(strict_types=1);

namespace App\Service\GoogleSheet;

use Google_Exception;
use Google_Service_Sheets_AppendValuesResponse;

interface GoogleSheetClientInterface
{
    /**
     * @param array $values
     * @return Google_Service_Sheets_AppendValuesResponse
     * @throws Google_Exception
     */
    public function write(array $values): Google_Service_Sheets_AppendValuesResponse;
}