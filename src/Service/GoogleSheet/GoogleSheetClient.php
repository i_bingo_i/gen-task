<?php

declare(strict_types=1);

namespace App\Service\GoogleSheet;

use Doctrine\Migrations\Configuration\Exception\FileNotFound;
use Google_Client;
use Google_Exception;
use Google_Service_Sheets;
use Google_Service_Sheets_AppendValuesResponse;
use Google_Service_Sheets_ValueRange;
use Symfony\Component\HttpKernel\KernelInterface;

class GoogleSheetClient implements GoogleSheetClientInterface
{
    const APPLICATION_NAME = 'Test task';
    const ACCESS_TYPE = 'offline';
    const PURCHASES_SHEET_ID = '11ZUrlwCDigpKyok-NUPGMvKvVmRyVOQn0Ag4FmHlFwE';
    const APPEND_RANGE = 'A1';

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * GoogleSheetClient constructor.
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }


    /**
     * @inheritDoc
     */
    public function write(array $values): Google_Service_Sheets_AppendValuesResponse
    {
        return $this->service()->spreadsheets_values->append(
            self::PURCHASES_SHEET_ID,
            self::APPEND_RANGE,
            $this->body($values),
            $this->parameters()
        );
    }

    /**
     * @throws Google_Exception
     */
    private function client(): Google_Client
    {
        $client = new Google_Client();
        $client->setApplicationName(self::APPLICATION_NAME);
        $client->setScopes([Google_Service_Sheets::SPREADSHEETS]);
        $client->setAccessType(self::ACCESS_TYPE);
        $client->setAuthConfig($this->getAuthConfig());

        return $client;
    }

    /**
     * @throws Google_Exception
     */
    private function service(): Google_Service_Sheets
    {
        return new Google_Service_Sheets($this->client());
    }

    /**
     * @return string
     * @throws FileNotFound
     */
    private function getAuthConfig(): string
    {
        if (true === file_exists($this->kernel->getProjectDir() . '/Google Sheets-4d0777a443c3.json')) {
            return $this->kernel->getProjectDir() . '/Google Sheets-4d0777a443c3.json';
        }

        throw new FileNotFound('Config file for Google Sheet un exist');
    }

    /**
     * @param array $values
     * @return Google_Service_Sheets_ValueRange
     */
    private function body(array $values): Google_Service_Sheets_ValueRange
    {
        return new Google_Service_Sheets_ValueRange(
            [
                'values' => $values
            ]
        );
    }

    /**
     * @return array
     */
    private function parameters(): array
    {
        return [
            'valueInputOption' => 'RAW',
            'insertDataOption' => 'INSERT_ROWS'
        ];
    }
}