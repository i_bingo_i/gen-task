<?php

declare(strict_types=1);

namespace App\Message;

interface GoogleSheetsMessageInterface
{
    /**
     * @return int
     */
    public function getPurchaseId(): int;
}