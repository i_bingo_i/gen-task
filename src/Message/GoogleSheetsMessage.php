<?php

declare(strict_types=1);

namespace App\Message;

class GoogleSheetsMessage implements GoogleSheetsMessageInterface
{
    /**
     * @var int
     */
    private $purchaseId;

    /**
     * GoogleSheetsMessage constructor.
     * @param int $purchaseId
     */
    public function __construct(int $purchaseId)
    {
        $this->purchaseId = $purchaseId;
    }

    /**
     * @inheritDoc
     */
    public function getPurchaseId(): int
    {
        return $this->purchaseId;
    }
}