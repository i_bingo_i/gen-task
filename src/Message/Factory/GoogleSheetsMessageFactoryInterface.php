<?php

declare(strict_types=1);

namespace App\Message\Factory;

use App\Message\GoogleSheetsMessageInterface;

interface GoogleSheetsMessageFactoryInterface
{
    /**
     * @param int $purchaseId
     * @return GoogleSheetsMessageInterface
     */
    public function create(int $purchaseId): GoogleSheetsMessageInterface;
}