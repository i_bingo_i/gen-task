<?php

declare(strict_types=1);

namespace App\Message\Factory;

use App\Message\GoogleSheetsMessage;
use App\Message\GoogleSheetsMessageInterface;

class GoogleSheetsMessageFactory implements GoogleSheetsMessageFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(int $purchaseId): GoogleSheetsMessageInterface
    {
        return new GoogleSheetsMessage($purchaseId);
    }
}