<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\ValidatorBuilder;

class PurchaseCreateValidator implements PurchaseCreateValidatorInterface
{
    /**
     * @var ValidatorBuilder
     */
    private $validatorBuilder;

    /**
     * PurchaseCreateValidator constructor.
     * @param ValidatorBuilder $validatorBuilder
     */
    public function __construct(ValidatorBuilder $validatorBuilder)
    {
        $this->validatorBuilder = $validatorBuilder;
    }

    /**
     * @inheritDoc
     */
    public function validate(array $inputData): void
    {
        $validator = $this->validatorBuilder->getValidator();

        $errors = $validator->validate($inputData, $this->getConstrains());

        if (false === empty($errors->count())) {
            $messages = null;
            foreach ($errors as $error) {
                $messages .= $error->getPropertyPath() . ' : ' . $error->getMessage() . PHP_EOL;
            }

            throw new ValidatorException($messages);
        }
    }

    /**
     * @return Collection
     */
    private function getConstrains(): Collection
    {
        return new Collection([
            'name' => [new Required(), new NotBlank(), new Type(['type' => 'string'])],
            'email' => [new Required(),  new NotBlank(), new Email()]
        ]);
    }
}