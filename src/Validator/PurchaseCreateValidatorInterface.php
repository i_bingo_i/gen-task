<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Exception\ValidatorException;

interface PurchaseCreateValidatorInterface
{
    /**
     * @param array $inputData
     * @return void
     */
    public function validate(array $inputData): void;
}