<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Astrologist;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Astrologist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Astrologist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Astrologist[]    findAll()
 * @method Astrologist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AstrologistRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Astrologist::class);
    }
}
