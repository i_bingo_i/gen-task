# Test task

## Requirements
* PHP 7.3
* Mysql 5.7.29
* Symfony framework

## Installation

##### Clone project
> git clone https://i_bingo_i@bitbucket.org/i_bingo_i/gen-task.git

##### Copy .env file from .env.dist

> `cp .env.dist .env`

##### Deploy environment
> `docker-compose build`

> `docker-compose up -d`

##### Installing composer dependencies
> `docker-compose exec php composer install`

##### Run migrations
> `docker-compose exec php bin/console doctrine:migrations:migrate`

##### Ren fixtures
> `docker-compose exec php bin/console doctrine:fixtures:load`

## API Documentation

https://documenter.getpostman.com/view/11049645/SzezbBPH?version=latest

## Link to the Google Sheet table

https://drive.google.com/open?id=11ZUrlwCDigpKyok-NUPGMvKvVmRyVOQn0Ag4FmHlFwE

